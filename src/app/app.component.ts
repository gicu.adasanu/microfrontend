import {Component, OnInit} from '@angular/core';
import {Employee} from './model/Employee';
import {HttpRequest} from './services/HttpRequest';
import {NzMessageService} from 'ng-zorro-antd/message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'MicroFrontend';
  listOfEmployee: Employee[] = [];
  isVisible: boolean = false;
  type: string = '';
  tmpSelectedEmployee: Employee = new Employee();
  name: string = '';
  surname: string = '';
  post: string = '';

  constructor(private httpReq: HttpRequest, private message: NzMessageService) {}

  ngOnInit(): void {
    this.loadListOfEmployee();
  }

  loadListOfEmployee() {
    this.httpReq.getEmployeeList().subscribe(
      response => {
        this.listOfEmployee = response;
      },
      error => {
        this.createMessage(`error`, error.message)
      });
  }

  createMessage(type: string, error: string): void {
    this.message.create(type, error, {
      nzDuration: 5000
    });
  }

  showModal(type: string, e: Employee) {
    this.type = type;
    this.isVisible = true;
    this.tmpSelectedEmployee = e;
    if (this.type == 'read') {
      this.httpReq.readEmployee(e).subscribe(
        response => {
          this.tmpSelectedEmployee = response;
        }, error => {
          this.createMessage(`error`, error.message);
          this.handleCancel();
        }
      );
    }

    if (type == 'update') {
      this.name = this.tmpSelectedEmployee.name;
      this.surname = this.tmpSelectedEmployee.surname;
      this.post = this.tmpSelectedEmployee.post;
    }
  }

  handleCancel() {
    this.isVisible = false;
    this.type = '';
    this.tmpSelectedEmployee = new Employee();
    this.resetNgModel();
  }

  handleOk() {
    if (this.type == 'delete') {
      this.httpReq.deleteEmployee(this.tmpSelectedEmployee.name, this.tmpSelectedEmployee.surname).subscribe(
        response => {

        },error => {
          this.createMessage(`error`, error.message);
        }
      )
    }

    if (this.type == 'new') {
      let e = new Employee();
      e.name = this.name;
      e.surname = this.surname;
      e.post = this.post;
      console.log(e);
      this.httpReq.addEmployee(e).subscribe(
        response => {},
        error => {
          this.createMessage(`error`, error.message);
        }
      );
    }

    if (this.type == 'update') {
      let e = new Employee();
      e.name = this.name;
      e.surname = this.surname;
      e.post = this.post;
      this.httpReq.updateEmployee(this.tmpSelectedEmployee, e).subscribe(
        response =>{},
        error => {
          this.createMessage(`error`, error.message);
        }
      );
    }

    this.isVisible = false;
    this.type = '';
    this.tmpSelectedEmployee = new Employee();
    this.resetNgModel();
    this.loadListOfEmployee();
  }

  resetNgModel() {
    this.name = '';
    this.surname = '';
    this.post = '';
  }
}
