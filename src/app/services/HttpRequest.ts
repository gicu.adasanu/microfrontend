import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// @ts-ignore
import {Observable} from 'rxjs';
import {Employee} from '../model/Employee';

// @ts-ignore
@Injectable({
  providedIn: 'root'
})
export class HttpRequest {
  api: string = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  getEmployeeList(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.api}/employee/list`);
  }

  deleteEmployee(name: string, surname: string) {
    return this.http.delete(`${this.api}/employee/delete?name=` + name + `&surname=` + surname);
  }

  updateEmployee(e: Employee, newEmployee: Employee) {
    return this.http.post(`${this.api}/employee/update?name=` + e.name + `&surname=` + e.surname
      + `&newName=` + newEmployee.name + `&newSurname=` + newEmployee.surname + `&newPost=` + newEmployee.post, '');
  }

  readEmployee(e: Employee): Observable<Employee> {
    return this.http.get<Employee>(`${this.api}/employee/read?name=` + e.name + `&surname=` + e.surname);
  }

  addEmployee(e: Employee) {
    return this.http.put(`${this.api}/employee/create?name=` + e.name + `&surname=` + e.surname + '&post=' + e.post, '');
  }
}
